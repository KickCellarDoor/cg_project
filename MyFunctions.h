//
// Created by cxxnzb on 2016/10/29.
//

#ifndef OPENGL_PROJECT_MYFUNCTIONS_H
#define OPENGL_PROJECT_MYFUNCTIONS_H
#pragma once

#include <GL/glut.h>
#include <cmath>
#include <vector>
#include <stack>

using namespace std;

void MyglVertex2i(int x1, int y1, int x2, int y2); //draw line with DDA

void MyCircle(int x, int y,int r); //中点生成算法8路画圆 Bresenham改进算法

void MyOval(int x,int y,int a,int b); //中点生成算法4路画椭圆 Bresenham改进算法

void MyPolygon(vector<int> xi,vector<int> yi);   //多边形生成，a为边长，b为边数

void cover(int x,int y);

void cc();


#endif //OPENGL_PROJECT_MYFUNCTIONS_H
