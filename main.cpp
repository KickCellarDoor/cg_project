#include <iostream>

#include "MyFunctions.h"
bool mouseLeftDown;
bool mouseRightDown;
float mouseX,mouseY;
float Start_mouseX = 250, Start_mouseY = 250;
float End_mouseX = 250, End_mouseY = 250;
float cameraDistance;
float cameraAngleX;
float cameraAngleY;
vector<int> xi,yi;
int current_shape;
int num = 0;
void reset(){
    Start_mouseX = 0;
    Start_mouseY = 0;
    End_mouseX = 0;
    End_mouseY = 0;
    mouseX = 0;
    mouseY = 0;
}
void displayCB(void){
    if(current_shape != 5)
    glClear(GL_COLOR_BUFFER_BIT);
    //glFlush();
    glColor3f(1.0,1.0,1.0);		/* set current color to white */
    glPointSize(1);
    glBegin(GL_POINTS);/* draw filled triangle */


    switch (current_shape){
        case 1:
            MyglVertex2i(End_mouseX,End_mouseY,Start_mouseX,Start_mouseY);
            break;
        case 2:
            if(Start_mouseX <= 0 || Start_mouseY <= 0 || End_mouseX <=0 || End_mouseY <= 0)
                break;
            MyCircle((Start_mouseX + End_mouseX)/2,(Start_mouseY + End_mouseY)/2,0.5 * sqrt((End_mouseX - Start_mouseX) * (End_mouseX - Start_mouseX) + (End_mouseY - Start_mouseY) * (End_mouseY - Start_mouseY)));
            break;
        case 3:
            if(Start_mouseX <= 0 || Start_mouseY <= 0 || End_mouseX <=0 || End_mouseY <= 0)
                break;
            MyOval((Start_mouseX+End_mouseX)/2,(Start_mouseY+End_mouseY)/2,abs(End_mouseX-Start_mouseX)/2,abs(End_mouseY - Start_mouseY)/2);
            break;
        case 4:
            MyPolygon(xi,yi);

            xi.erase(xi.begin(),xi.end());
            yi.erase(yi.begin(),yi.end());
            current_shape = -1;
            break;
        case 5:
            cover(Start_mouseX,Start_mouseY);
            break;
        case -1:
            for(int i = 0;i < xi.size();i++){
                glVertex2d(xi[i],yi[i]);
            }
            break;
        default:
            break;
    }

    glEnd();				/* OpenGL draws the filled triangle */
    glFlush();				/* Complete any pending operations */
}

void keyCB(unsigned char key, int x, int y)	/* called on key press */{
    switch (key){
        case 'q':exit(0);
            break;
        case 'c':current_shape = 2;
            break;
        case 'l':current_shape = 1;
            break;
        case 'p':current_shape = -1;
            break;
        case 'o':current_shape = 3;
            break;
        case 'r':num = 0;
            glClear(GL_COLOR_BUFFER_BIT);
            cc();
            break;
        case 'e':
            current_shape = 4;
            break;
        case 't':
            current_shape = 5;
            break;
        default:
            break;
    }
    if(key >= '0' && key <= '9'){
        int temp = key - '0' + 0;
        num = num * 10 + temp;
    }
    if(key != 'e')
        reset();
    if(key == 'r' || key == 'e'){
        glutPostRedisplay();
    }
}

void MouseCB(int button,int state,int x,int y){
    mouseX = x;
    mouseY = 500 - y;
    if(button == GLUT_LEFT_BUTTON)
    {
        if(state == GLUT_DOWN)
        {
            mouseLeftDown = true;
            Start_mouseX = x;
            Start_mouseY = 500-y;
            if(current_shape == -1){
                xi.push_back(x);
                yi.push_back(500-y);
            }
        }
        else if(state == GLUT_UP) {
            End_mouseX = x;
            End_mouseY = 500-y;
            mouseLeftDown = false;
        }
    }

    else if(button == GLUT_RIGHT_BUTTON)
    {
        if(state == GLUT_DOWN)
        {
            mouseRightDown = true;
        }
        else if(state == GLUT_UP)
            mouseRightDown = false;
    }
    glutPostRedisplay();
}
void MouseMotionCB(int x, int y) {
    if(mouseLeftDown)
    {
        End_mouseY = 500 - y;
        End_mouseX = x;
        cameraAngleY += (x - mouseX);
        cameraAngleX += (500 - y - mouseY);
        mouseX = x;
        mouseY = 500 - y;
    }
    if(mouseRightDown)
    {
        cameraDistance += (500 - y - mouseY) * 0.2f;
        mouseY = 500 - y;
    }
    glutPostRedisplay();
}

int main(int argc, char *argv[]) {
    int win;

    glutInit(&argc, argv);		/* initialize GLUT system */

    glutInitDisplayMode(GLUT_RGB | GLUT_SINGLE);
    glutInitWindowSize(500,500);		/* width=400pixels height=500pixels */
    win = glutCreateWindow("MyTest");	/* create window */

    /* from this point on the current window is win */

    glClearColor(0.0,0.0,0.0,0.0);	/* set background to black */
    gluOrtho2D(0,500,0,500);		/* how object is mapped to window */
    glutDisplayFunc(displayCB);		/* set window's display callback */
    //glClearColor(0.0,0.0,0.0,0.0);

    glutKeyboardFunc(keyCB);		/* set window's key callback */
    glutMouseFunc(MouseCB);
    glutMotionFunc(MouseMotionCB);
    glutMainLoop();			/* start processing events... */

    /* execution never reaches this point */

    return 0;
}
