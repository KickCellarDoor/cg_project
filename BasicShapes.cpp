//
// Created by cxxnzb on 2016/10/29.
//
#include <iostream>
#include "MyFunctions.h"
static int map[500][500] = {0};

void cc(){
    for(int i = 0;i < 500;i++){
        for(int j = 0;j < 500;j++)
            map[i][j] = 0;
    }
}

void MyglVertex2i(int x1, int y1, int x2, int y2){
    GLdouble  k,start_x,start_y,end_x,end_y,d_x,d_y;

    d_x = x2 - x1;
    d_y = y2 - y1;


    if(fabs(d_x) > fabs(d_y)){
        k = d_y / d_x;

        if(d_x > 0) {
            start_x = x1;
            start_y = y1;
            end_x = x2;
        }
        else{
            start_x = x2;
            start_y = y2;
            end_x = x1;
        }

        while(start_x < end_x){
            glVertex2d(start_x,start_y);
            map[(int)start_x][(int)start_y] = 1;
            start_x ++;
            start_y += k;
        }
    }
    else{
        k = d_x / d_y;

        if(d_y > 0) {
            start_y = y1;
            start_x = x1;
            end_y = y2;
        }
        else{
            start_y = y2;
            start_x = x2;
            end_y = y1;
        }

        while(start_y < end_y){
            glVertex2d(start_x,start_y);
            map[(int)start_x][(int)start_y] = 1;
            start_y ++;
            start_x += k;
        }
    }
}

void MyCircle(int x, int y, int r){
    cc();
    GLdouble end_y;
    int d, start_x, start_y;
    glVertex2d(x,y+r);
    glVertex2d(x,y-r);
    glVertex2d(x-r,y);
    glVertex2d(x+r,y);
    map[x][y+r] = 1;
    map[x][y-r] = 1;
    map[x-r][y] = 1;
    map[x+r][y] = 1;
    start_x = 0;
    start_y = r;
    d = 3 - r * 2;

    end_y = sqrt(2) / 2 * r;

    while(start_y >= end_y){     //八分法画圆
        glVertex2d(x + start_x,y + start_y);
        map[(int)(x+start_x)][(int)(y+start_y)] = 1;
        glVertex2d(x - start_x, y + start_y);
        map[(int)(x-start_x)][(int)(y+start_y)] = 1;
        glVertex2d(x + start_x,y - start_y);
        map[(int)(x+start_x)][(int)(y-start_y)] = 1;
        glVertex2d(x - start_x,y - start_y);
        map[(int)(x-start_x)][(int)(y-start_y)] = 1;
        glVertex2d(x + start_y,y + start_x);
        map[(int)(x+start_y)][(int)(y+start_x)] = 1;
        glVertex2d(x - start_y,y + start_x);
        map[(int)(x-start_y)][(int)(y+start_x)] = 1;
        glVertex2d(x + start_y,y - start_x);
        map[(int)(x+start_y)][(int)(y-start_x)] = 1;
        glVertex2d(x - start_y,y - start_x);
        map[(int)(x-start_y)][(int)(y-start_x)] = 1;

        if(d < 0){
            d = d + start_x * 4 + 6;
        }
        else{
            d = d + (start_x - start_y) * 4+ 10;
            start_y--;
        }

        start_x++;
    }
}

void MyOval(int x,int y,int a,int b){
    cc();
    int a2, b2, start_x, start_y , d;
    GLdouble end_x;
    a2 = a * a;
    b2 = b * b;
    start_x = 0;
    start_y = b;

    glVertex2d(x,y+b);
    glVertex2d(x,y-b);
    glVertex2d(x-a,y);
    glVertex2d(x+a,y);
    map[x][y+b] = 1;
    map[x][y-b] = 1;
    map[x-a][y] = 1;
    map[x+a][y] = 1;
    end_x = a2 / sqrt(a2+b2);

    //斜率为1前以x为基准画
    d = 2 * b2 - 2 * b * a2 + a2;
    while(start_x <= end_x){
        if(d < 0)
        {
            d += 2 * b2 * (2 * start_x + 3);
        }
        else
        {
            d += 2 * b2 * (2 * start_x + 3) - 4 * a2 * (start_y - 1);
            start_y--;
        }

        start_x++;

        glVertex2i(x + start_x,y + start_y);
        map[(int)(x+start_x)][(int)(y+start_y)] = 1;
        glVertex2i(x + start_x,y - start_y);
        map[(int)(x+start_x)][(int)(y-start_y)] = 1;
        glVertex2i(x - start_x,y + start_y);
        map[(int)(x-start_x)][(int)(y+start_y)] = 1;
        glVertex2i(x - start_x,y - start_y);
        map[(int)(x-start_x)][(int)(y-start_y)] = 1;
    }

    //斜率到达1后以y为基准画
    d = b2 * (start_x * start_x + start_x) + a2 * (start_y * start_y - start_y) - a2 * b2;
    while(start_y >= 0){
        glVertex2i(x + start_x,y + start_y);
        map[(int)(x+start_x)][(int)(y+start_y)] = 1;
        glVertex2i(x + start_x,y - start_y);
        map[(int)(x+start_x)][(int)(y-start_y)] = 1;
        glVertex2i(x - start_x,y + start_y);
        map[(int)(x-start_x)][(int)(y+start_y)] = 1;
        glVertex2i(x - start_x,y - start_y);
        map[(int)(x-start_x)][(int)(y-start_y)] = 1;
        start_y--;
        if(d < 0){
            start_x++;
            d -= 2 * a2 * start_y + a2 - 2 * b2 * start_x - 2 * b2;
        } else{
            d -= 2 * a2 * start_y + a2;
        }
    }
}

void MyPolygon(vector<int> xi,vector<int> yi){
    cc();
    for(int i = 0;i < xi.size()-1;i++){
        MyglVertex2i(xi[i],yi[i],xi[i+1],yi[i+1]);
    }
    MyglVertex2i(xi[0],yi[0],xi[xi.size()-1],yi[yi.size()-1]);
}

void cover(int x,int y){
    stack<vector<int> > points;
    vector<int> temp(2);
    temp[0] = x;
    temp[1] = y;
    points.push(temp);


    while(!points.empty()) {
        vector<int> a;
        a = points.top();
        points.pop();
        if(a[0] == 0|| a[0] == 499 || a[1] == 0 || a[1] == 499)
            continue;
        glVertex2d(a[0],a[1]);
        map[a[0]][a[1]] = 1;

        int ip = 0;


        ip = map[a[0]-1][a[1]];
        if(ip == 0){
            vector<int> temp(2);
            temp[0] = a[0] - 1;
            temp[1] = a[1];
            points.push(temp);
        }

        ip = map[a[0]+1][a[1]];
        if(ip == 0){
            vector<int> temp(2);
            temp[0] = a[0] + 1;
            temp[1] = a[1];
            points.push(temp);
        }

        ip = map[a[0]][a[1]-1];
        if(ip == 0){
            vector<int> temp(2);
            temp[0] = a[0];
            temp[1] = a[1] - 1;
            points.push(temp);
        }

        ip = map[a[0]][a[1]+1];
        if(ip == 0){
            vector<int> temp(2);
            temp[0] = a[0];
            temp[1] = a[1] + 1;
            points.push(temp);
        }

    }

}